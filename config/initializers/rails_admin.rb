RailsAdmin.config do |config|

  ### Popular gems integration

  ## == Devise ==
  config.authenticate_with do
    warden.authenticate! scope: :user
  end
  config.current_user_method(&:current_user)

  ## == CancanCan ==
  # config.authorize_with :cancancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  ## == Gravatar integration ==
  ## To disable Gravatar integration in Navigation Bar set to false
  # config.show_gravatar = true
  config.model Customer do
   
    list do
      field  :id
      field  :name
      field  :email
      field  :api_key
      field  :city
      field  :active
   
    end
  end

  config.model Order do
    create do
      field  :cliente_id
      field  :ctt_id
      field  :dna_hawb
      field  :ccusto_id
      field  :tipo_enc_id
      field  :prod_flash_id
      field  :frq_rec_id
      field  :ctt_ter_id
      field  :num_cliente
      field  :codigo_lote
      field  :id_local_rem
      field  :customer_name
   
      # field :user_id, :hidden do
      #   default_value do
      #     bindings[:view]._current_user.id
      #   end
      # end
    end
   
    edit do
      field  :cliente_id
      field  :ctt_id
      field  :dna_hawb
      field  :ccusto_id
      field  :tipo_enc_id
      field  :prod_flash_id
      field  :frq_rec_id
      field  :ctt_ter_id
      field  :num_cliente
      field  :codigo_lote
      field  :id_local_rem
      field  :customer_name

    end
   
    list do
      field  :customer_name
      field  :customer_email
      field  :cliente_id
      field  :remetente
      field  :destinatario
      field  :cpf
      field  :cnpj
      field  :cidade
      field  :uf
      field  :cep
      field  :codigo_lote
      field  :peso
      field  :quantidade
      field  :valor
      field  :chave_nf
   
    end
  end

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end
end
