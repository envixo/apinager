# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_12_23_133938) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", id: :serial, force: :cascade do |t|
    t.string "namespace", limit: 255
    t.text "body"
    t.string "resource_id", limit: 255, null: false
    t.string "resource_type", limit: 255, null: false
    t.integer "author_id"
    t.string "author_type", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace"
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"
  end

  create_table "admin_users", id: :serial, force: :cascade do |t|
    t.string "email", limit: 255, default: "", null: false
    t.string "encrypted_password", limit: 255, default: "", null: false
    t.string "reset_password_token", limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip", limit: 255
    t.string "last_sign_in_ip", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "name", limit: 255
    t.index ["email"], name: "index_admin_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true
  end

  create_table "api_request_counts", id: :serial, force: :cascade do |t|
    t.integer "customer_id"
    t.date "date_request"
    t.integer "count"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["customer_id"], name: "index_api_request_counts_on_customer_id"
  end

  create_table "capital_products", id: :serial, force: :cascade do |t|
    t.integer "deadline_min"
    t.integer "deadline_max"
    t.integer "capital_id", null: false
    t.integer "product_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["capital_id"], name: "index_capital_products_on_capital_id"
    t.index ["product_id"], name: "index_capital_products_on_product_id"
  end

  create_table "capitals", id: :serial, force: :cascade do |t|
    t.string "capital"
    t.string "parameterized_capital"
    t.integer "state_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["state_id"], name: "index_capitals_on_state_id"
  end

  create_table "cep_range_ignoreds", id: :serial, force: :cascade do |t|
    t.string "code_start"
    t.string "code_end"
    t.integer "customer_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["customer_id"], name: "index_cep_range_ignoreds_on_customer_id"
  end

  create_table "cep_zones", id: :serial, force: :cascade do |t|
    t.string "name", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "product_id"
  end

  create_table "cep_zones_states", id: false, force: :cascade do |t|
    t.integer "cep_zone_id", null: false
    t.integer "state_id", null: false
    t.index ["cep_zone_id", "state_id"], name: "index_cep_zones_states_on_cep_zone_id_and_state_id"
    t.index ["state_id", "cep_zone_id"], name: "index_cep_zones_states_on_state_id_and_cep_zone_id"
  end

  create_table "ceps", id: :serial, force: :cascade do |t|
    t.string "city", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "state_id"
    t.integer "code_start"
    t.integer "code_end"
    t.string "subsidiary", limit: 255
    t.string "region"
    t.string "parameterized_city"
    t.index ["state_id"], name: "index_ceps_on_state_id"
  end

  create_table "ceps_products", id: :serial, force: :cascade do |t|
    t.integer "cep_id"
    t.integer "product_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["cep_id"], name: "index_ceps_products_on_cep_id"
    t.index ["product_id"], name: "index_ceps_products_on_product_id"
  end

  create_table "customers", id: :serial, force: :cascade do |t|
    t.string "name", limit: 255
    t.string "api_key", limit: 255
    t.string "city", limit: 255
    t.integer "state_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "email", limit: 255
    t.boolean "active", default: true
    t.integer "platform_id"
    t.boolean "frenet", default: false
    t.integer "kind"
    t.integer "status"
    t.index ["state_id"], name: "index_customers_on_state_id"
  end

  create_table "customers_products", id: :serial, force: :cascade do |t|
    t.integer "customer_id"
    t.integer "product_id"
    t.float "discount", default: 0.0
    t.boolean "increase", default: false
    t.index ["customer_id"], name: "index_customers_products_on_customer_id"
    t.index ["product_id"], name: "index_customers_products_on_product_id"
  end

  create_table "delayed_jobs", id: :serial, force: :cascade do |t|
    t.integer "priority", default: 0, null: false
    t.integer "attempts", default: 0, null: false
    t.text "handler", null: false
    t.text "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string "locked_by"
    t.string "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["priority", "run_at"], name: "delayed_jobs_priority"
  end

  create_table "email_logs", id: :serial, force: :cascade do |t|
    t.boolean "status"
    t.string "destinatarios"
    t.string "assunto"
    t.date "data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "open_email"
  end

  create_table "inland_products", id: :serial, force: :cascade do |t|
    t.integer "inland_id"
    t.integer "product_id"
    t.integer "deadline_min"
    t.integer "deadline_max"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["inland_id"], name: "index_inland_products_on_inland_id"
    t.index ["product_id"], name: "index_inland_products_on_product_id"
  end

  create_table "inlands", id: :serial, force: :cascade do |t|
    t.string "city"
    t.string "parameterized_city"
    t.integer "deadline_min"
    t.integer "deadline_max"
    t.integer "state_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["state_id"], name: "index_inlands_on_state_id"
  end

  create_table "locations", id: :serial, force: :cascade do |t|
    t.integer "product_id"
    t.integer "first_start_range"
    t.integer "first_end_range"
    t.float "first_price_range"
    t.integer "second_start_range"
    t.integer "second_end_range"
    t.float "second_price_range"
    t.integer "multiplier_range"
    t.float "multiplier_price"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "cep_zone_id"
    t.float "extra_weight_price"
    t.index ["cep_zone_id"], name: "index_locations_on_cep_zone_id"
    t.index ["product_id"], name: "index_locations_on_product_id"
  end

  create_table "orders", force: :cascade do |t|
    t.integer "cliente_id"
    t.integer "ctt_id"
    t.integer "dna_hawb"
    t.integer "ccusto_id"
    t.integer "tipo_enc_id"
    t.integer "prod_flash_id"
    t.string "frq_rec_id"
    t.integer "ctt_ter_id"
    t.string "num_enc_cli"
    t.string "num_cliente"
    t.string "remetente"
    t.string "destinatario"
    t.string "logradouro"
    t.string "bairro"
    t.string "numero"
    t.string "complemento"
    t.string "cidade"
    t.string "uf"
    t.integer "cep"
    t.string "fone"
    t.string "fone_recado"
    t.string "codigo_lote"
    t.string "peso"
    t.string "quantidade"
    t.string "valor"
    t.integer "id_local_rem"
    t.string "cpf"
    t.string "cnpj"
    t.string "ie"
    t.string "obs1"
    t.string "obs2"
    t.string "obs3"
    t.string "obs4"
    t.string "chave_nf"
    t.bigint "customer_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "kind"
    t.integer "status"
    t.index ["customer_id"], name: "index_orders_on_customer_id"
  end

  create_table "platforms", id: :serial, force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.string "instruction_integration"
  end

  create_table "product_rms", id: :serial, force: :cascade do |t|
    t.integer "deadline_min"
    t.integer "deadline_max"
    t.integer "rm_id", null: false
    t.integer "product_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id"], name: "index_product_rms_on_product_id"
    t.index ["rm_id"], name: "index_product_rms_on_rm_id"
  end

  create_table "products", id: :serial, force: :cascade do |t|
    t.string "name", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "weight_limit"
    t.integer "cep_id"
    t.string "param_name", limit: 255
    t.integer "deadline_min"
    t.integer "deadline_max"
    t.datetime "last_upload_at"
    t.index ["cep_id"], name: "index_products_on_cep_id"
  end

  create_table "products_states", id: :serial, force: :cascade do |t|
    t.integer "deadline_min"
    t.integer "deadline_max"
    t.integer "product_id", null: false
    t.integer "state_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id"], name: "index_products_states_on_product_id"
    t.index ["state_id"], name: "index_products_states_on_state_id"
  end

  create_table "range_locations", id: :serial, force: :cascade do |t|
    t.integer "location_id"
    t.integer "start_weight"
    t.integer "end_weight"
    t.decimal "price", precision: 8, scale: 2
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["location_id"], name: "index_range_locations_on_location_id"
  end

  create_table "rms", id: :serial, force: :cascade do |t|
    t.string "city"
    t.string "parameterized_city"
    t.integer "state_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["state_id"], name: "index_rms_on_state_id"
  end

  create_table "states", id: :serial, force: :cascade do |t|
    t.string "name", limit: 255
    t.integer "cep_zone_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "capital"
    t.string "parameterized_capital"
    t.index ["cep_zone_id"], name: "index_states_on_cep_zone_id"
  end

  create_table "test_zipcodes", id: :serial, force: :cascade do |t|
    t.string "city", null: false
    t.integer "state_id"
    t.string "zipcode", limit: 8, null: false
    t.boolean "serviced_by_the_network", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["state_id"], name: "index_test_zipcodes_on_state_id"
  end

  create_table "uploads", id: :serial, force: :cascade do |t|
    t.string "upload_file"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "document"
    t.integer "kind"
    t.integer "status"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "capitals", "states"
  add_foreign_key "cep_range_ignoreds", "customers"
  add_foreign_key "inlands", "states"
  add_foreign_key "orders", "customers"
  add_foreign_key "rms", "states"
  add_foreign_key "test_zipcodes", "states"
end
