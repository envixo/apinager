class AddKindToOrder < ActiveRecord::Migration[6.1]
  def change
    add_column :orders, :kind, :integer
  end
end
