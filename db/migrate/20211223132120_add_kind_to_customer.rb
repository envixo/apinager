class AddKindToCustomer < ActiveRecord::Migration[6.1]
  def change
    add_column :customers, :kind, :integer
  end
end
