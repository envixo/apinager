class CreateOrders < ActiveRecord::Migration[6.1]
  def change
    create_table :orders do |t|
      t.integer :cliente_id
      t.integer :ctt_id
      t.integer :dna_hawb
      t.integer :ccusto_id
      t.integer :tipo_enc_id
      t.integer :prod_flash_id
      t.string :frq_rec_id
      t.integer :ctt_ter_id
      t.string :num_enc_cli
      t.string :num_cliente
      t.string :remetente
      t.string :destinatario
      t.string :logradouro
      t.string :bairro
      t.string :numero
      t.string :complemento
      t.string :cidade
      t.string :uf
      t.integer :cep
      t.string :fone
      t.string :fone_recado
      t.string :codigo_lote
      t.string :peso
      t.string :quantidade
      t.string :valor
      t.integer :id_local_rem
      t.string :cpf
      t.string :cnpj
      t.string :ie
      t.string :obs1
      t.string :obs2
      t.string :obs3
      t.string :obs4
      t.string :chave_nf
      t.references :customer, null: false, foreign_key: true

      t.timestamps
    end
  end
end
